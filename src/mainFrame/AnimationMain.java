package mainFrame;

import javax.swing.*;

import games.Game;
import games.animationGame.*;

public class AnimationMain {
    private static final int FRAME_WIDTH = 300;
    private static final int FRAME_HEIGHT = 400;
    private static JFrame frame;
    private static Game game;

    public static void main(String [] args) throws InterruptedException {
        makeJFrame();
        fetchAnimationGame();
        setFrameSettings();
        runGame();
    }

    private static void makeJFrame() {
        frame = new JFrame("Ping Pong Game");
    }
    private static void fetchAnimationGame() {
        game = games.animationGame.AnimationGame.getAnimationGame(FRAME_WIDTH, FRAME_HEIGHT);
    }
    private static AnimationGamePanel getAnimationGamePanel(){
        return games.animationGame.AnimationGame.getAnimationGamePanel();
    }
    private static void setFrameSettings() {
        frame.add(getAnimationGamePanel());
        frame.setSize(FRAME_WIDTH + 50, FRAME_HEIGHT + 50);
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
    private static void runGame() throws InterruptedException{
        while(true){
            ((AnimationGame)game).applyBonusesForPoints();
            game.repaint();
            Thread.sleep(6);
        }

    }


}
