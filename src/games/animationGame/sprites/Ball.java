package games.animationGame.sprites;

import games.animationGame.AnimationGamePanel;

import java.awt.*;
import java.util.Random;

public class Ball {

    private final int BALL_WIDTH;
    private final int BALL_HEIGHT;
    private final int BALL_INITIAL_X = 1;
    private final int BALL_INITIAL_Y = 1;
    private int ballX;
    private int ballY;
    private double ballSpeed;
    private final double BALL_INITIAL_SPEED = 1;
    private final double BALL_MIN_SPEED = 1;
    private final double BALL_MAX_SPEED = 4;
    private double ballXChange;
    private final double BALL_INITIAL_X_CHANGE = 1.;
    private final double BALL_MAX_X_CHANGE = 2;
    private double ballYChange;
    private final double BALL_INITIAL_Y_CHANGE = 1.;
    private final double BALL_MAX_Y_CHANGE = 2;



    private final double CORNER_HIT_VERTICAL_SPEED_INCREASE_RATE = 1.4;
    public double getBALL_INITIAL_Y_CHANGE() {
        return BALL_INITIAL_Y_CHANGE;
    }
    public double getBALL_INITIAL_X_CHANGE() {
        return BALL_INITIAL_X_CHANGE;
    }
    public int getBALL_INITIAL_X() {
        return BALL_INITIAL_X;
    }
    public int getBALL_INITIAL_Y() {
        return BALL_INITIAL_Y;
    }
    public double getBALL_INITIAL_SPEED() {
        return BALL_INITIAL_SPEED;
    }
    public double getBALL_MAX_SPEED() {
        return BALL_MAX_SPEED;
    }
    public double getBALL_MIN_SPEED() {
        return BALL_MIN_SPEED;
    }

    private static Ball instance = null;
    private AnimationGamePanel animationGamePanel;

    public double getBallXChange() {
        return ballXChange;
    }
    public void setBallXChange(double ballXChange) {
        this.ballXChange = ballXChange;
    }
    public double getBallYChange() {
        return ballYChange;
    }
    public void setBallYChange(double ballYChange) {
        this.ballYChange = ballYChange;
    }
    public int getBallX() {
        return ballX;
    }
    public void setBallX(int ballX) {
        this.ballX = ballX;
    }
    public int getBallY() {
        return ballY;
    }
    public void setBallY(int ballY) {
        this.ballY = ballY;
    }
    public double getBallSpeed() {
        return ballSpeed;
    }
    public void setBallSpeed(double ballSpeed) {
        this.ballSpeed = ballSpeed;
    }
    public int getBALL_WIDTH() {
        return BALL_WIDTH;
    }
    public int getBALL_HEIGHT() {
        return BALL_HEIGHT;
    }

    private Ball(AnimationGamePanel gamePanel){
        animationGamePanel = gamePanel;
        ballX = BALL_INITIAL_X;
        ballY = BALL_INITIAL_Y;
        ballSpeed = BALL_INITIAL_SPEED;
        ballXChange = BALL_INITIAL_X_CHANGE;
        ballYChange = BALL_INITIAL_Y_CHANGE;
        BALL_WIDTH = Math.min(gamePanel.getFrameWidth(),gamePanel.getFrameHeight())/10;
        BALL_HEIGHT = BALL_WIDTH;

    }


    public static Ball getInstance(AnimationGamePanel gamePanel){
        if ( instance == null ){
            instance = new Ball(gamePanel);
        }
        return instance;
    }


    public void paint(Graphics2D g2d){
        moveBall();
        g2d.setColor(Color.BLUE);
        g2d.fillOval(ballX, ballY, BALL_WIDTH, BALL_HEIGHT);

    }

    public boolean ballMovesLeft(){
        return ballXChange < 0;
    }
    public boolean ballMovesRight(){
        return ballXChange > 0;
    }
    public boolean ballMovesUp(){
        return ballYChange < 0;
    }
    public boolean ballMovesDown(){
        return ballYChange > 0;
    }
    public boolean ballMovesVertically(){
        return ballXChange == 0;
    }
    public void switchVerticalDirection(){
        ballYChange = -ballYChange;
    }
    public void switchHorizontalDirection(){
        ballXChange = -ballXChange;
    }

    public void multiplyVerticalSpeedBy(double multiplyBy){
        ballYChange = Math.min( BALL_MAX_Y_CHANGE , ballYChange * multiplyBy);
        ballYChange = Math.max(-BALL_MAX_Y_CHANGE,ballYChange);
    }

    public void addToVerticalSpeedBy(double addVertical){
        ballYChange = Math.min( BALL_MAX_Y_CHANGE, ballYChange + addVertical);
        ballYChange = Math.max(BALL_MAX_Y_CHANGE, ballYChange);

    }

    public void multiplyHorizontalSpeedBy(double multiplyBy){
        ballXChange = Math.min(BALL_MAX_X_CHANGE, ballXChange * multiplyBy);
        ballXChange = Math.max(-BALL_MAX_X_CHANGE, ballXChange);
    }

    public void addToHorizontalSpeed(double addHorizontal){
        ballXChange = Math.min(BALL_MAX_X_CHANGE, ballXChange + addHorizontal);
        ballXChange = Math.max(-BALL_MAX_X_CHANGE, ballXChange);
    }


    public void moveBall(){

        ballX += (int)(ballXChange * ballSpeed);
        ballY += (int)(ballYChange * ballSpeed);

        int oldPoints = animationGamePanel.getAnimationGame().getPoints();
        int newPoints;

        if ( ballIsHitByPad() ) {
            animationGamePanel.getAnimationGame().setPoints((int) Math.floor(oldPoints + ballSpeed / 2 + 1));
            newPoints = animationGamePanel.getAnimationGame().getPoints();
            for (int i = oldPoints+1; i <= newPoints; i++) {
                if (i % 10 == 0) {
                    animationGamePanel.getAnimationGame().eachTenPointsSound.play();
                    break;
                }
            }
        }


        if ( ballX + BALL_WIDTH > animationGamePanel.getFrameWidth() ) {
            ballX = 2 * (animationGamePanel.getFrameWidth()-BALL_WIDTH) - ballX;
            switchHorizontalDirection();
        }
        if ( ballX < 0 ){
            ballX = - ballX;
            switchHorizontalDirection();
        }

        if ( ballY < 0 ){
            ballY = -ballY;
            switchVerticalDirection();
        }

        if ( ballY + BALL_HEIGHT > animationGamePanel.getFrameHeight() ){
            ballY = 2 * (animationGamePanel.getFrameHeight()-BALL_HEIGHT) - ballY;
            ballYChange = -ballYChange;
            animationGamePanel.getAnimationGame().setPoints( (int) Math.floor(animationGamePanel.getAnimationGame().getPoints() - BALL_MAX_SPEED + ballSpeed - 1));
            animationGamePanel.getAnimationGame().setLostPoint(true);
            animationGamePanel.getAnimationGame().lostPointsSound.play();
        }

    }

    public boolean ballIsHitByPad(){
        Random rand = new Random();
        Pad pad = animationGamePanel.getPad();

        if (animationGamePanel.getAnimationGame().padHitsBallStraight() && ballMovesDown()) {
            switchVerticalDirection();
            addToHorizontalSpeed( Math.signum( ballXChange ));
            addToHorizontalSpeed( pad.getPadXChange() / 3. * Math.abs(ballXChange));
            animationGamePanel.getAnimationGame().straightHitSound.play();
            return true;
        }

        if (animationGamePanel.getAnimationGame().padHitsBallWithTheLeftCorner() && ballMovesDown() ){
            switchVerticalDirection();
            multiplyHorizontalSpeedBy(CORNER_HIT_VERTICAL_SPEED_INCREASE_RATE);
            addToHorizontalSpeed( pad.getPadXChange() / 3. * Math.abs(ballXChange));
            if ( ballMovesRight() ){
                switchHorizontalDirection();
            }
            animationGamePanel.getAnimationGame().cornerHitSound.play();
            return true;
        }

        if (animationGamePanel.getAnimationGame().padHitsBallWithTheRightCorner() && ballMovesDown()){
            ballYChange = -ballYChange;
            multiplyHorizontalSpeedBy(CORNER_HIT_VERTICAL_SPEED_INCREASE_RATE);
            addToHorizontalSpeed( pad.getPadXChange() / 3. * Math.abs(ballXChange));
            if (ballMovesLeft()) {
                switchHorizontalDirection();
            }
            animationGamePanel.getAnimationGame().cornerHitSound.play();

            return true;
        }
        return false;
    }
    public boolean isSpeedBelowMax(){
        return ballSpeed < BALL_MAX_SPEED;
    }

    public boolean speedIsAtMostOneLessThanMax(){
        return ballSpeed < BALL_MAX_SPEED - 1;
    }

    public boolean isSpeedAboveMin(){
        return ballSpeed > BALL_MIN_SPEED;
    }

    public boolean speedIsAtLeastAtMinPlusOne(){
        return ballSpeed >= BALL_MIN_SPEED + 1;
    }

}
