package games.animationGame.sprites;

import games.animationGame.AnimationGamePanel;

import java.awt.*;

public class Pad {

    private final int PANEL_PAD_Y_POSITION;
    private double padXSpeed;
    private double PAD_MAX_X_SPEED = 6;
    private double PAD_MIN_X_SPEED = 1;
    private double padXChange;
    private int padX;
    private int padWidth;
    private int padHeight;
    private AnimationGamePanel animationGamePanel;
    private final int DEFAULT_PAD_WIDTH;
    private final int MIN_PAD_WIDTH = 30;
    private final int MAX_PAD_WIDTH = 100;
    private final int DEFAULT_PAD_HEIGHT = 15;
    private static Pad instance = null;

    public double getPAD_MAX_X_SPEED() {
        return PAD_MAX_X_SPEED;
    }
    public double getPAD_MIN_X_SPEED() {
        return PAD_MIN_X_SPEED;
    }
    public int getMIN_PAD_WIDTH() {
        return MIN_PAD_WIDTH;
    }
    public int getMAX_PAD_WIDTH() {
        return MAX_PAD_WIDTH;
    }
    public int getPANEL_PAD_Y_POSITION() {
        return PANEL_PAD_Y_POSITION;
    }
    public double getPadXSpeed() {
        return padXSpeed;
    }
    public void setPadXSpeed(double padXSpeed) {
        this.padXSpeed = padXSpeed;
    }
    public double getPadXChange() {
        return padXChange;
    }
    public void setPadXChange(double padXChange) {
        this.padXChange = padXChange;
    }
    public int getPadX() {
        return padX;
    }
    public void setPadX(int padX) {
        this.padX = padX;
    }
    public int getPadWidth() {
        return padWidth;
    }
    public void setPadWidth(int padWidth) {
        this.padWidth = padWidth;
    }
    public int getPadHeight() {
        return padHeight;
    }
    public void setPadHeight(int padHeight) {
        this.padHeight = padHeight;
    }


    private Pad(AnimationGamePanel gamePanel){
        animationGamePanel = gamePanel;
        DEFAULT_PAD_WIDTH = Math.min(Math.max(animationGamePanel.getFrameWidth()/8, MIN_PAD_WIDTH), MAX_PAD_WIDTH);
        PANEL_PAD_Y_POSITION = gamePanel.getFrameHeight() - DEFAULT_PAD_HEIGHT;
        padX = gamePanel.getFrameWidth()/2;
        padXChange = 0;
        padXSpeed = PAD_MIN_X_SPEED;
        padWidth = DEFAULT_PAD_WIDTH;
        padHeight = DEFAULT_PAD_HEIGHT;

    }

    public static Pad getInstance(AnimationGamePanel animationGamePanel){
        if (instance == null){
            instance = new Pad(animationGamePanel);
        }
        return instance;
    }

    public void paint(Graphics2D g2d){
        movePad();
        g2d.setColor(new Color(100,200,155));
        g2d.fillRect(padX, PANEL_PAD_Y_POSITION, padWidth, padHeight);

    }


    public void movePad(){
        padX += (int)(padXChange * padXSpeed);
        if (padX + padWidth >= animationGamePanel.getFrameWidth()){
            padX = animationGamePanel.getFrameWidth() - padWidth ;
        }
        if ( padX < 0 ){
            padX = 0;
        }

    }



}
