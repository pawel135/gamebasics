package games.animationGame;

import games.Game;
import games.animationGame.sprites.Ball;
import games.animationGame.sprites.Pad;

import java.applet.Applet;
import java.applet.AudioClip;
import java.awt.event.KeyListener;
import java.awt.event.KeyEvent;
import java.net.MalformedURLException;
import java.net.URL;

public class AnimationGame extends Game {
    private final static int DEFAULT_FRAME_WIDTH = 300;
    private final static int DEFAULT_FRAME_HEIGHT = 400;
    private static AnimationGame gameInstance = null;

    private static AnimationGamePanel animationGamePanel;

    private boolean lostPoint = false;
    private int points = 0;
    private final int MIN_POINTS = 0;
    private final int MAX_POINTS = 100;
    private final int POINTS_TO_GAME_OVER = -8;

    public AudioClip cornerHitSound;
    public AudioClip straightHitSound;
    public AudioClip eachTenPointsSound;
    public AudioClip gameOverSound;
    public AudioClip backgroundSound;
    public AudioClip lostPointsSound;

    public int getPoints() {
        return points;
    }
    public void setPoints(int points) {
        this.points = points;
    }
    public boolean isLostPoint() {
        return lostPoint;
    }
    public void setLostPoint(boolean lostPoint) {
        this.lostPoint = lostPoint;
    }


    private AnimationGame(int frameWidth, int frameHeight){

        animationGamePanel = new AnimationGamePanel(this,frameWidth, frameHeight);
        try {
            straightHitSound = Applet.newAudioClip(new URL("file:/home/pawel13/Downloads/IntelliJ/IdeaProjects/GameBasics/sounds/hit.wav"));
            cornerHitSound = Applet.newAudioClip(new URL("file:/home/pawel13/Downloads/IntelliJ/IdeaProjects/GameBasics/sounds/lostPoints.wav"));
            lostPointsSound = Applet.newAudioClip(new URL("file:/home/pawel13/Downloads/IntelliJ/IdeaProjects/GameBasics/sounds/corner_hit.wav"));
            eachTenPointsSound = Applet.newAudioClip(new URL("file:/home/pawel13/Downloads/IntelliJ/IdeaProjects/GameBasics/sounds/each_ten_points.wav"));
            gameOverSound = Applet.newAudioClip(new URL("file:/home/pawel13/Downloads/IntelliJ/IdeaProjects/GameBasics/sounds/gameOver.wav"));
            backgroundSound = Applet.newAudioClip(new URL("file:/home/pawel13/Downloads/IntelliJ/IdeaProjects/GameBasics/sounds/backgroundUkulele.wav"));
        } catch (MalformedURLException e){
            e.printStackTrace();
        }
        backgroundSound.loop();
    }

    public static AnimationGame getAnimationGame(){
        if (gameInstance == null) {
            return AnimationGame.getAnimationGame(DEFAULT_FRAME_WIDTH, DEFAULT_FRAME_HEIGHT);
        }
        return gameInstance;
    }

    public static AnimationGame getAnimationGame(int frameWidth, int frameHeight){
        if (gameInstance == null){
            gameInstance =  new AnimationGame(frameWidth, frameHeight);
        }
        return gameInstance;
    }



    public static AnimationGamePanel getAnimationGamePanel(){
        return animationGamePanel;
    }



    @Override
    public void repaint(){
        animationGamePanel.repaint();
    }


    public boolean padHitsBallStraight(){
       return ballIsOnPadHeight() && ballIsWithinPadWidth();
    }
    public boolean padHitsBallWithTheLeftCorner(){

        return ballIsOnPadHeight() && ballIsWithinPadLeftCornerWidth();
    }

    public boolean padHitsBallWithTheRightCorner(){
        return ballIsOnPadHeight() && ballIsWithinPadRightCornerWidth();
    }


    public boolean ballIsOnPadHeight() {

        Ball ball = animationGamePanel.getBall();
        Pad pad = animationGamePanel.getPad();

        return ( ball.getBallY() + ball.getBALL_HEIGHT() <=
                pad.getPANEL_PAD_Y_POSITION() + pad.getPadHeight()/2
                && ball.getBallY() + ball.getBALL_HEIGHT() > pad.getPANEL_PAD_Y_POSITION()
                );
    }

    public boolean ballIsWithinPadWidth (){
        Ball ball = animationGamePanel.getBall();
        Pad pad = animationGamePanel.getPad();
        return ( ball.getBallX() + ball.getBALL_WIDTH()/2 >= pad.getPadX()
                && ball.getBallX() + ball.getBALL_WIDTH()/2 <= pad.getPadX() + pad.getPadWidth()
                );


    }

    public boolean ballIsWithinPadLeftCornerWidth (){
        Ball ball = animationGamePanel.getBall();
        Pad pad = animationGamePanel.getPad();

        return  ( ball.getBallX() >= pad.getPadX() - ball.getBALL_WIDTH()/2 -
                    Math.sqrt( Math.max(0, ball.getBALL_WIDTH()/2*pad.getPadHeight()-Math.pow(pad.getPadHeight(),2)/4 ) )
                && ball.getBallX() < pad.getPadX() - ball.getBALL_WIDTH()/2
        );
    }

    public boolean ballIsWithinPadRightCornerWidth() {
        Ball ball = animationGamePanel.getBall();
        Pad pad = animationGamePanel.getPad();

        return  ( ball.getBallX() <= pad.getPadX() + pad.getPadWidth() -
                    (ball.getBALL_WIDTH()/2 - Math.sqrt( Math.max(0, ball.getBALL_WIDTH()/2*pad.getPadHeight()-Math.pow(pad.getPadHeight(),2)/4 ) ) )
                && ball.getBallX() > pad.getPadX() + pad.getPadWidth() - ball.getBALL_WIDTH()/2
                );

    }


    public void applyBonusesForPoints() {
        //TODO:

        Ball ball = animationGamePanel.getBall();
        Pad pad = animationGamePanel.getPad();

        if ( points <= POINTS_TO_GAME_OVER ) {
            gameOverSound.play();
            animationGamePanel.gameOver();
        }

        pad.setPadXSpeed(
                animationGamePanel.isGamePaused() ? 0 :
                Math.max(pad.getPAD_MIN_X_SPEED(),
                        Math.min(pad.getPAD_MAX_X_SPEED(),
                            pad.getPAD_MIN_X_SPEED() + ((pad.getPAD_MAX_X_SPEED()-pad.getPAD_MIN_X_SPEED())*points / (MAX_POINTS-MIN_POINTS)) )
                )
        );

        pad.setPadWidth(
            Math.max(pad.getMIN_PAD_WIDTH(),
                Math.min(pad.getMAX_PAD_WIDTH(),
                    pad.getMAX_PAD_WIDTH() + (( pad.getMIN_PAD_WIDTH() - pad.getMAX_PAD_WIDTH() ) * points / ( MAX_POINTS - MIN_POINTS ) )
                )
            )
        );

        ball.setBallSpeed(
                animationGamePanel.isGamePaused() ? 0 :
                animationGamePanel.getAdditionalSpeed() +
                Math.max(ball.getBALL_MIN_SPEED(),
                    Math.min(ball.getBALL_MAX_SPEED(),
                        ball.getBALL_MIN_SPEED() + ( ( ball.getBALL_MAX_SPEED() - ball.getBALL_MIN_SPEED() ) * points / (MAX_POINTS - MIN_POINTS) )
                    )
                )
        );

        System.out.println("padXSpeed: " + pad.getPadXSpeed() + ", padWidth " + pad.getPadWidth() + ", ballSpeed: " + ball.getBallSpeed() );
     }



}

