package games.animationGame;

import games.animationGame.sprites.Ball;
import games.animationGame.sprites.Pad;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;


public class AnimationGamePanel extends JPanel {
    private int frameWidth;
    private int frameHeight;
    private AnimationGame animationGame;
    private Ball ball;
    private Pad pad;
    private int additionalSpeed = 0;
    private boolean gamePaused = false;


    public Pad getPad() {
        return pad;
    }
    public Ball getBall() {
        return ball;
    }
    public int getFrameHeight() {
        return frameHeight;
    }
    public int getFrameWidth() {
        return frameWidth;
    }
    public AnimationGame getAnimationGame() {
        return animationGame;
    }
    public int getAdditionalSpeed(){
        return additionalSpeed;
    }
    public boolean isGamePaused() {
        return gamePaused;
    }

    public AnimationGamePanel(AnimationGame animationGame, int frameWidth, int frameHeight){
        super();
        this.frameWidth = frameWidth;
        this.frameHeight = frameHeight;
        this.animationGame = animationGame;
        System.out.println("frame created: " + frameWidth + " " + frameHeight);
        ball = Ball.getInstance(this);
        pad = Pad.getInstance(this);
        setUpKeyboardListener();
    }



    @Override
    public void paint(Graphics g){
        super.paint(g);
        Graphics2D g2d = (Graphics2D) g;
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING,RenderingHints.VALUE_ANTIALIAS_ON);

        ball.paint(g2d);
        pad.paint(g2d);
        paintBorders(g2d);
        paintScore(g2d);
    }

    private void paintScore(Graphics2D g2d){
        g2d.setFont(Font.getFont("Arial"));
        g2d.setColor(Color.BLACK);
        g2d.drawString(Integer.toString(getAnimationGame().getPoints()),frameWidth/2,frameHeight/10);
    }

    private void paintBorders(Graphics2D g2d) {
        g2d.setColor(Color.GRAY);
        g2d.drawRect(0, 0, frameWidth, frameHeight);
    }




    public void setUpKeyboardListener(){
        KeyListener listener = new MyKeyListener();
        addKeyListener(listener);
        setFocusable(true);
    }

    class MyKeyListener implements KeyListener {


        @Override
        public void keyTyped(KeyEvent e) {

        }

        @Override
        public void keyPressed(KeyEvent e) {
            switch (e.getKeyCode()){
                case KeyEvent.VK_LEFT:
                    pad.setPadXChange(-1);
                    break;
                case KeyEvent.VK_RIGHT:
                    pad.setPadXChange(1);
                    break;
                case KeyEvent.VK_UP:
                    if (ball.speedIsAtMostOneLessThanMax() && !gamePaused ){
                        additionalSpeed++;
                        //ball.setBallSpeed(ball.getBallSpeed() + 1);
                    }
                    break;
                case KeyEvent.VK_DOWN:
                    if (ball.speedIsAtLeastAtMinPlusOne() && !gamePaused ) {
                       additionalSpeed--;
                        // ball.setBallSpeed(ball.getBallSpeed() - 1);
                    }
                    break;
                case KeyEvent.VK_P:
                    if ( gamePaused ){
                        gamePaused = false;
                    }
                    else {
                        gamePaused = true;
                    }
                    break;
                default:
                    pad.setPadXChange(0);
                    break;
            }
            System.out.println("Pressed " + KeyEvent.getKeyText(e.getKeyCode()) + ", padXChange = " + pad.getPadXChange() + ", ballSpeed" + ball.getBallSpeed() );
            System.out.println("PadX position: " + pad.getPadX());
        }

        @Override
        public void keyReleased(KeyEvent e) {
            switch (e.getKeyCode()){
                case KeyEvent.VK_LEFT:
                    if (pad.getPadXChange() < 0){
                        pad.setPadXChange(0);
                    }
                    break;
                case KeyEvent.VK_RIGHT:
                    if (pad.getPadXChange() > 0) {
                        pad.setPadXChange(0);
                    }
                    break;
            }
            System.out.println("Released " + KeyEvent.getKeyText(e.getKeyCode()) + ", padXChange = " + pad.getPadXChange() + ", ballSpeed = " + ball.getBallSpeed() );

        }
    }

    public void gameOver(){
        JOptionPane.showMessageDialog(this, "GameOver", "Game Over!", JOptionPane.OK_OPTION);

        System.exit(ABORT);

    }


}
